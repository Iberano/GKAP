-module(bellmannford_tests).
-author("Andre Soblechero").
-author("Eike Meyer").

-include_lib("eunit/include/eunit.hrl").

circle_d_test() ->
    G = adtgraph:createG(d),

V1 = adtgraph:addEdge(G, 1, 2),
V2 = adtgraph:addEdge(V1, 2, 3),
V3 = adtgraph:addEdge(V2, 3, 4),
V4 = adtgraph:addEdge(V3, 4, 5),
V5 = adtgraph:addEdge(V4, 5, 1),

V6 = adtgraph:setAtE(V5, 1, 2, weight, 1),
V7 = adtgraph:setAtE(V6, 2, 3, weight, 1),
V8 = adtgraph:setAtE(V7, 3, 4, weight, 1),
V9 = adtgraph:setAtE(V8, 4, 5, weight, 1),
V10 = adtgraph:setAtE(V9, 5, 1, weight, 1),

bellmannford:bellmannford(V10, 1, d).

circle_ud_test() ->
    G = adtgraph:createG(ud),

    V1 = adtgraph:addEdge(G, 1, 2),
    V2 = adtgraph:addEdge(V1, 2, 3),
    V3 = adtgraph:addEdge(V2, 3, 4),
    V4 = adtgraph:addEdge(V3, 4, 5),
    V5 = adtgraph:addEdge(V4, 5, 1),

    V6 = adtgraph:setAtE(V5, 1, 2, weight, 1),
    V7 = adtgraph:setAtE(V6, 2, 3, weight, 1),
    V8 = adtgraph:setAtE(V7, 3, 4, weight, 1),
    V9 = adtgraph:setAtE(V8, 4, 5, weight, 1),
    V10 = adtgraph:setAtE(V9, 5, 1, weight, 1),

    bellmannford:bellmannford(V10, 1, ud).

shortcut_d_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 1, 3),
        V3 = adtgraph:addEdge(V2, 2, 3),

        V4 = adtgraph:setAtE(V3, 1, 2, weight, 25),
        V5 = adtgraph:setAtE(V4, 2, 3, weight, 1),
        V6 = adtgraph:setAtE(V5, 1, 3, weight, 1),

        bellmannford:bellmannford(V6,1, d).

shortcut_ud_test() ->
    G = adtgraph:createG(ud),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 1, 3),
        V3 = adtgraph:addEdge(V2, 2, 3),

        V4 = adtgraph:setAtE(V3, 1, 2, weight, 25),
        V5 = adtgraph:setAtE(V4, 2, 3, weight, 1),
        V6 = adtgraph:setAtE(V5, 1, 3, weight, 1),

        bellmannford:bellmannford(V6,1, ud).

negative_circle_d_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 2, 3),
        V3 = adtgraph:addEdge(V2, 3, 1),

        V4 = adtgraph:setAtE(V3, 1, 2, weight, 1),
        V5 = adtgraph:setAtE(V4, 2, 3, weight, 2),
        V6 = adtgraph:setAtE(V5, 3, 1, weight, -4),

        bellmannford:bellmannford(V6, 1, d).