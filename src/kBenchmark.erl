-module(kBenchmark).
-author("Andre Soblechero").
-author("Eike Meyer").

-export([createKn/0, measureBellmanFordLineD/0, measureBellmanFordLineUD/0, measureDijkstraLineD/0, measureDijkstraLineUD/0]).

createKn() ->
gengraph:gengraph(5,4,4,1,1,'K5.graph'), %% 190
gengraph:gengraph(6,5,5,1,1,'K6.graph'),
gengraph:gengraph(7,6,6,1,1,'K7.graph'),
gengraph:gengraph(8,7,7,1,1,'K8.graph'),
gengraph:gengraph(9,8,8,1,1,'K9.graph'),
gengraph:gengraph(10,9,9,1,1,'K10.graph'),
gengraph:gengraph(11,10,10,1,1,'K11.graph'),
gengraph:gengraph(12,11,11,1,1,'K12.graph'),
gengraph:gengraph(13,12,12,1,1,'K13.graph'),
gengraph:gengraph(14,99,99,1,1,'K14.graph').

%% bellmanford directed
measureBellmanFordLineD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('K5', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('K6', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('K7', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('K8', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('K9', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('K10', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('K11', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('K12', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('K13', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('K100', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% bellmanford undirected
measureBellmanFordLineUD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('K5', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('K6', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('K7', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('K8', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('K9', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('K10', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('K11', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('K12', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('K13', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('K14', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line directed
measureDijkstraLineD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('K5', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('K6', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('K7', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('K8', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('K9', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('K10', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('K11', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('K12', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('K13', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('K14', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line undirected
measureDijkstraLineUD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('K5', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('K6', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('K7', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('K8', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('K9', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('K10', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('K11', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('K12', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('K13', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('K14', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).