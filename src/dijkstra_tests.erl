-module(dijkstra_tests).
-author("Andre Soblechero").
-author("Eike Meyer").

-include_lib("eunit/include/eunit.hrl").

tiny_D_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G,1,2),
        V2 = adtgraph:addEdge(V1,2,3),

        V3 = adtgraph:setAtE(V2, 1, 2, "weight", 10),
        V4 = adtgraph:setAtE(V3, 2, 3, "weight", 15),

        dijkstra:dijkstra(V4, 1 , d).

small_D_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G,1,2),
        V2 = adtgraph:addEdge(V1,1,3),
        V3 = adtgraph:addEdge(V2,1,4),
        V4 = adtgraph:addEdge(V3,2,4),

        V5 = adtgraph:setAtE(V4, 1, 2, "weight", 3),
        V6 = adtgraph:setAtE(V5, 1, 3, "weight", 1),
        V7 = adtgraph:setAtE(V6, 1, 4, "weight", 20),
        V8 = adtgraph:setAtE(V7, 2, 4,"weight", 10),

        dijkstra:dijkstra(V8, 1, d).

circle_d_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 2, 3),
        V3 = adtgraph:addEdge(V2, 3, 4),
        V4 = adtgraph:addEdge(V3, 4, 5),
        V5 = adtgraph:addEdge(V4, 5, 1),

        V6 = adtgraph:setAtE(V5, 1, 2, "weight", 1),
        V7 = adtgraph:setAtE(V6, 2, 3, "weight", 1),
        V8 = adtgraph:setAtE(V7, 3, 4, "weight", 1),
        V9 = adtgraph:setAtE(V8, 4, 5, "weight", 1),
        V10 = adtgraph:setAtE(V9, 5, 1, "weight", 1),

        dijkstra:dijkstra(V10, 1, d).

circle_ud_test() ->
    G = adtgraph:createG(ud),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 2, 3),
        V3 = adtgraph:addEdge(V2, 3, 4),
        V4 = adtgraph:addEdge(V3, 4, 5),
        V5 = adtgraph:addEdge(V4, 5, 1),

        V6 = adtgraph:setAtE(V5, 1, 2, "weight", 1),
        V7 = adtgraph:setAtE(V6, 2, 3, "weight", 1),
        V8 = adtgraph:setAtE(V7, 3, 4, "weight", 1),
        V9 = adtgraph:setAtE(V8, 4, 5, "weight", 1),
        V10 = adtgraph:setAtE(V9, 5, 1, "weight", 1),

        dijkstra:dijkstra(V10, 1, ud).

check_gengraph_kVier_test() ->
    G = adtgraph:importG('graph_06', d),

    dijkstra:dijkstra(G, 1, d).

printG_graph_06_test() ->
   A=adtgraph:importG('graph_06',d),
     adtgraph:printG(A,'Testgraph').

shortcut_d_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 1, 3),
        V3 = adtgraph:addEdge(V2, 2, 3),

        V4 = adtgraph:setAtE(V3, 1, 2, weight, 25),
        V5 = adtgraph:setAtE(V4, 2, 3, weight, 1),
        V6 = adtgraph:setAtE(V5, 1, 3, weight, 1),

        dijkstra:dijkstra(V6,1, d).

shortcut_ud_test() ->
    G = adtgraph:createG(ud),

        V1 = adtgraph:addEdge(G, 1, 2),
        V2 = adtgraph:addEdge(V1, 1, 3),
        V3 = adtgraph:addEdge(V2, 2, 3),

        V4 = adtgraph:setAtE(V3, 1, 2, weight, 25),
        V5 = adtgraph:setAtE(V4, 2, 3, weight, 1),
        V6 = adtgraph:setAtE(V5, 1, 3, weight, 1),

        dijkstra:dijkstra(V6,1, ud).

keineNachfolger_test() ->
    G = adtgraph:createG(d),

        V1 = adtgraph:addVertex(G,1),
        V2 = adtgraph:addEdge(V1, 2, 3),

        V3 = adtgraph:setAtE (V2, 2, 3, weight, 25),

        dijkstra:dijkstra(V3, 3, d).

printG_test() ->
   A=adtgraph:importG('graph_2_50',d),
     adtgraph:printG(A,'LinieGraph').


