%%%-------------------------------------------------------------------
%%% @author andre soblechero
%%% @author Eike Meyer
%%% @copyright (C) 2018,
%%% @doc
%%%
%%% @end
%%% Created : 15. Apr 2018 18:28
%%%-------------------------------------------------------------------
-module(dijkstra).
-author("andre").

%% API
-export([dijkstra/3, benchmark/5, benchmark/4, runAllBenchmarks/0]).


buildOutput(_Graph, []) -> [];

buildOutput(Graph, [ID | Rest]) ->
  {Source, Weight, _} = adtgraph:getValV(Graph, ID, weight),
  [{ID, Weight, Source} | buildOutput(Graph, Rest)].



initAllVertex(Graph, []) -> Graph;

initAllVertex(Graph, [ID | Rest]) -> initAllVertex(adtgraph:setAtV(Graph, ID, weight, {nil, inf, false}), Rest).


prepareGraph(Graph, StartVertex) ->
  VertexList = adtgraph:getVertices(Graph),
  NewGraph = initAllVertex(Graph, VertexList),
  adtgraph:setAtV(NewGraph, StartVertex, weight, {StartVertex, 0, true}).





addToPrioQueue({_Source, ID, Weight}, []) -> [{ID, Weight}];

addToPrioQueue({_Source1, ID, Weight}, [{ID, Weight} | Rest]) -> [{ID, Weight} | Rest];

addToPrioQueue({_Source, ID, Weight}, [{ID, Weight2} | Rest]) when Weight < Weight2 ->
  [{ID,Weight} | Rest];

addToPrioQueue({_Source, ID, Weight}, [{ID, Weight2} | Rest]) when Weight > Weight2 ->
  [{ID,Weight2} | Rest];

addToPrioQueue({Source, ID, Weight}, [{ID2, Weight2} | Rest]) ->
  [{ID2, Weight2} | addToPrioQueue({Source, ID, Weight}, Rest)].



addListToPrioQueue(_, [], []) -> nil;

addListToPrioQueue(_, [], Queue) -> Queue;

addListToPrioQueue(PreparedGraph, [{Source, ID, Weight} | Rest], Queue) ->
  A = {Source, ID, Weight},

  case adtgraph:getValV(PreparedGraph, ID, weight) of
    {_, _, false} -> addListToPrioQueue(PreparedGraph, Rest, addToPrioQueue(A, Queue));
    {_, _, true} -> addListToPrioQueue(PreparedGraph, Rest, Queue)
  end.





prepareTarget(_Graph, _Source, [], _SourceEntf) ->
  [];

prepareTarget(Graph, Source, [ID | Rest], SourceEntf) ->
  [{Source, ID, adtgraph:getValE(Graph, Source, ID, weight) + SourceEntf} | prepareTarget(Graph, Source, Rest, SourceEntf)].



setNeighbours(Graph, []) -> Graph;

setNeighbours(Graph, [{Source, ID, EntfFromStart} | Rest]) ->
  {_VSource, Entf, OK} = adtgraph:getValV(Graph, ID, weight),
  case Entf of
    inf -> setNeighbours(adtgraph:setAtV(Graph, ID, weight, {Source, EntfFromStart, OK}), Rest);
    _Else ->
      case Entf > EntfFromStart of
        true -> setNeighbours(adtgraph:setAtV(Graph, ID, weight, {Source, EntfFromStart, OK}), Rest);
        false -> setNeighbours(Graph, Rest)
      end
  end.




getMinOfList([],A)->A;

getMinOfList([{ID1,Weight1}|Rest],{_ID,Weight})when Weight1<Weight->getMinOfList(Rest,{ID1,Weight1});

getMinOfList([{_ID1,_Weight1}|Rest],{ID,Weight})->getMinOfList(Rest,{ID,Weight}).



deleteFromList([],_ID)->[];

deleteFromList([{ID,_Weight}|Rest],ID)->Rest;

deleteFromList([{ID1,Weight}|Rest],ID)->[{ID1,Weight}|deleteFromList(Rest,ID)].


dijkstra_rec(Graph, nil) -> Graph;

dijkstra_rec(Graph, []) -> Graph;

dijkstra_rec(Graph, [A|Rest]) ->
  List=[A|Rest],
  {ID,_Weight}=getMinOfList(Rest,A),
  RestOfQueue=deleteFromList(List,ID),
  {TrueSource, TrueEntf, false} = adtgraph:getValV(Graph, ID, weight),
  PreparedGraph = adtgraph:setAtV(Graph, ID, weight, {TrueSource, TrueEntf, true}),

  PreparedTarget = prepareTarget(PreparedGraph, ID, adtgraph:getTarget(PreparedGraph, ID), TrueEntf),
  NewGraph = setNeighbours(PreparedGraph, PreparedTarget),
  PrioQueue = addListToPrioQueue(NewGraph, PreparedTarget, RestOfQueue),
  %io:fwrite("~n~p~nGraph:~n~p~n~n", [PrioQueue, NewGraph]),
  dijkstra_rec(NewGraph, PrioQueue).




dijkstra(PreGraph, StartVertex, DUD) ->

  case util:type_is(PreGraph) of
    atom -> Graph = adtgraph:importG(PreGraph, DUD);
    tuple -> Graph = PreGraph
  end,

  VertexList = adtgraph:getVertices(Graph),

  PreparedGraph = prepareGraph(Graph, StartVertex),

  PreparedTarget = prepareTarget(PreparedGraph, StartVertex, adtgraph:getTarget(PreparedGraph, StartVertex), 0),

  NewGraph = setNeighbours(PreparedGraph, PreparedTarget),

  PrioQueue = addListToPrioQueue(NewGraph, PreparedTarget, []),

  %io:fwrite("~n~p~nGraph:~n~p~n~n", [PrioQueue, NewGraph]),
  case PrioQueue of
    nil -> buildOutput(NewGraph, VertexList);
    _Else -> buildOutput(dijkstra_rec(NewGraph, PrioQueue), VertexList)
  end.





getMeasuredMeanValue(_NewTree, _RandList, _DUD, Index, Last) when Index == 3 -> Last;

getMeasuredMeanValue(NewTree, RandList, DUD, Index, Last) ->
  RandElemGT = lists:nth(rand:uniform(length(RandList)), RandList),
  {GetTargetTime, _} = timer:tc(dijkstra, dijkstra, [NewTree, RandElemGT, DUD]),
  getMeasuredMeanValue(NewTree, RandList, DUD, Index + 1, Last + GetTargetTime).


benchmark(_X, _Y, 0, _DUD, _ResultsFile) -> ok;

benchmark(X, Y, INDEX, DUD, ResultsFile) ->
%generate Graph
  XX = round(X / 10),
  try gengraph:gengraph(X, 1, XX, 1, XX, "isse.graph")
  catch
    ok -> io:fwrite("Gengraph")

  end,
  io:fwrite("~p~n", [DUD]),
  NewTree = adtgraph:importG(isse, DUD),
  Edges = adtgraph:getEdges(NewTree),
  %measure runtime
  EdgesSum = length(Edges) / 2,
  VertexSum = length(adtgraph:getVertices(NewTree)),

  %io:fwrite("~nVetexSum: ~p EdgesSum: ~p~n",[VertexSum,EdgesSum]),
  io:fwrite("Start measuring...~n"),
  MeanValue = getMeasuredMeanValue(NewTree, Edges, DUD, 0, 0) / 3,
%get |Edges|,|Vertex|

  %Calculate predicted runtime
  PredictedTime = EdgesSum * VertexSum + VertexSum * VertexSum,
  io:fwrite(ResultsFile, "~p,~p,~p,~p~n", [VertexSum, EdgesSum, PredictedTime, MeanValue]),
  benchmark(X + Y, Y, INDEX - 1, DUD, ResultsFile).




benchmark(X, Y, INDEX, ud) ->
  DUD = ud,
  {ok, ResultsFile} = file:open("Dijkstra_undirected_results.csv", [write]),
  io:fwrite(ResultsFile, "|Vertecies|,|Edges|,erwartete Laufzeit,durchschnittliche Laufzeit~n", []),
  benchmark(X, Y, INDEX, DUD, ResultsFile),
  file:close(ResultsFile);


benchmark(X, Y, INDEX, d) ->
  DUD = d,
  {ok, ResultsFile} = file:open("Dijkstra_directed_results.csv", [write]),
  io:fwrite(ResultsFile, "|Vertecies|,|Edges|,erwartete Laufzeit,durchschnittliche Laufzeit~n", []),
  benchmark(X, Y, INDEX, DUD, ResultsFile),
  file:close(ResultsFile).


runAllBenchmarks() ->
  dijkstra:benchmark(20, 100, 7, d),
  dijkstra:benchmark(20, 100, 7, ud),
  bellmannford:benchmark(20, 100, 7, d),
  bellmannford:benchmark(20, 100, 7, ud)
.
