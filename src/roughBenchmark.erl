%%%-------------------------------------------------------------------
%%% @author Andre Soblechero, Eike Meyer
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. Apr 2018 16:23
%%%-------------------------------------------------------------------
-module(roughBenchmark).
-author("Andre Soblechero").
-author("Eike Meyer").

-export([createGraphs/0, createKn/0, measureDijkstraD/0, measureDijkstraKD/0, measureDijkstraUD/0, measurebellmannfordD/0, measurebellmannfordUD/0, createHeavyGraphs/0]).

createGraphs() ->
gengraph:gengraph(25,1,12,1,50,'E25.graph'),
gengraph:gengraph(50,1,25,1,50,'E50.graph'),
gengraph:gengraph(75,1,37,1,50,'E75.graph'),
gengraph:gengraph(100,1,50,1,50,'E100.graph'),
gengraph:gengraph(125,1,67,1,50,'E125.graph'),

gengraph:gengraph(150,1,75,1,50,'E150.graph'),
gengraph:gengraph(175,1,87,1,50,'E175.graph'),
gengraph:gengraph(200,1,100,1,50,'E200.graph'),
gengraph:gengraph(225,1,112,1,50,'E225.graph'),
gengraph:gengraph(250,1,125,1,50,'E250.graph').

createHeavyGraphs() ->
gengraph:gengraph(25,1,12,10000,100000,'HE25.graph'),
gengraph:gengraph(50,1,25,10000,100000,'HE50.graph'),
gengraph:gengraph(75,1,37,10000,100000,'HE75.graph'),
gengraph:gengraph(100,1,50,10000,100000,'HE100.graph'),
gengraph:gengraph(125,1,67,10000,100000,'HE125.graph'),

gengraph:gengraph(150,1,75,10000,100000,'HE150.graph'),
gengraph:gengraph(175,1,87,10000,100000,'HE175.graph'),
gengraph:gengraph(200,1,100,10000,100000,'HE200.graph'),
gengraph:gengraph(225,1,112,10000,100000,'HE225.graph'),
gengraph:gengraph(250,1,125,10000,100000,'HE250.graph').

createKn() ->
gengraph:gengraph(20,19,19,1,1,'K20.graph'), %% 190
gengraph:gengraph(28,27,27,1,1,'K28.graph'), %% 378
gengraph:gengraph(39,38,38,1,1,'K39.graph'), %% 741
gengraph:gengraph(54,53,53,1,1,'K54.graph'), %% 1431
gengraph:gengraph(76,75,75,1,1,'K76.graph'), %% 2850
gengraph:gengraph(106,105,105,1,1,'K106.graph'), %% 5565
gengraph:gengraph(150,149,149,1,1,'K150.graph'), %% 11175
gengraph:gengraph(213,212,212,1,1,'K20.graph'). %% 22350

measureDijkstraKD() ->

Start = erlang:timestamp(),
dijkstra:dijkstra('K20', 1, d),
Ende = erlang:timestamp(),
Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
io:fwrite("~p ~n", [Diffms]),

Start1 = erlang:timestamp(),
dijkstra:dijkstra('K28', 1, d),
Ende1 = erlang:timestamp(),
Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
io:fwrite("~p ~n", [Diffms1]),

Start2 = erlang:timestamp(),
dijkstra:dijkstra('K38', 1, d),
Ende2 = erlang:timestamp(),
Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
io:fwrite("~p ~n", [Diffms2]),

Start3 = erlang:timestamp(),
dijkstra:dijkstra('K54', 1, d),
Ende3 = erlang:timestamp(),
Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
io:fwrite("~p ~n", [Diffms3]),

Start4 = erlang:timestamp(),
dijkstra:dijkstra('K76', 1, d),
Ende4 = erlang:timestamp(),
Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
io:fwrite("~p ~n", [Diffms4]),

Start5 = erlang:timestamp(),
dijkstra:dijkstra('K106', 1, d),
Ende5 = erlang:timestamp(),
Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
io:fwrite("~p ~n", [Diffms5]),

Start6 = erlang:timestamp(),
dijkstra:dijkstra('K150', 1, d),
Ende6 = erlang:timestamp(),
Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
io:fwrite("~p ~n", [Diffms6]),

Start7 = erlang:timestamp(),
dijkstra:dijkstra('K213', 1, d),
Ende7 = erlang:timestamp(),
Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
io:fwrite("~p ~n", [Diffms7]).


measureDijkstraD() ->

Start = erlang:timestamp(),
dijkstra:dijkstra('E25', 1, d),
Ende = erlang:timestamp(),
Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
io:fwrite("~p ~n", [Diffms]),

Start1 = erlang:timestamp(),
dijkstra:dijkstra('E50', 1, d),
Ende1 = erlang:timestamp(),
Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
io:fwrite("~p ~n", [Diffms1]),

Start2 = erlang:timestamp(),
dijkstra:dijkstra('E75', 1, d),
Ende2 = erlang:timestamp(),
Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
io:fwrite("~p ~n", [Diffms2]),

Start3 = erlang:timestamp(),
dijkstra:dijkstra('E100', 1, d),
Ende3 = erlang:timestamp(),
Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
io:fwrite("~p ~n", [Diffms3]),

Start4 = erlang:timestamp(),
dijkstra:dijkstra('E125', 1, d),
Ende4 = erlang:timestamp(),
Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
io:fwrite("~p ~n", [Diffms4]),

Start5 = erlang:timestamp(),
dijkstra:dijkstra('E150', 1, d),
Ende5 = erlang:timestamp(),
Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
io:fwrite("~p ~n", [Diffms5]),

Start6 = erlang:timestamp(),
dijkstra:dijkstra('E175', 1, d),
Ende6 = erlang:timestamp(),
Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
io:fwrite("~p ~n", [Diffms6]),

Start7 = erlang:timestamp(),
dijkstra:dijkstra('E200', 1, d),
Ende7 = erlang:timestamp(),
Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
io:fwrite("~p ~n", [Diffms7]),

Start8 = erlang:timestamp(),
dijkstra:dijkstra('E225', 1, d),
Ende8 = erlang:timestamp(),
Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
io:fwrite("~p ~n", [Diffms8]),

Start9 = erlang:timestamp(),
dijkstra:dijkstra('E250', 1, d),
Ende9 = erlang:timestamp(),
Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
io:fwrite("~p ~n", [Diffms9]).


%% undirected
measureDijkstraUD() ->
Start = erlang:timestamp(),
dijkstra:dijkstra('E25', 1, ud),
Ende = erlang:timestamp(),
Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
io:fwrite("~p ~n", [Diffms]),

Start1 = erlang:timestamp(),
dijkstra:dijkstra('E50', 1, ud),
Ende1 = erlang:timestamp(),
Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
io:fwrite("~p ~n", [Diffms1]),

Start2 = erlang:timestamp(),
dijkstra:dijkstra('E75', 1, ud),
Ende2 = erlang:timestamp(),
Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
io:fwrite("~p ~n", [Diffms2]),

Start3 = erlang:timestamp(),
dijkstra:dijkstra('E100', 1, ud),
Ende3 = erlang:timestamp(),
Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
io:fwrite("~p ~n", [Diffms3]),

Start4 = erlang:timestamp(),
dijkstra:dijkstra('E125', 1, ud),
Ende4 = erlang:timestamp(),
Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
io:fwrite("~p ~n", [Diffms4]),

Start5 = erlang:timestamp(),
dijkstra:dijkstra('E150', 1, ud),
Ende5 = erlang:timestamp(),
Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
io:fwrite("~p ~n", [Diffms5]),

Start6 = erlang:timestamp(),
dijkstra:dijkstra('E175', 1, ud),
Ende6 = erlang:timestamp(),
Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
io:fwrite("~p ~n", [Diffms6]),

Start7 = erlang:timestamp(),
dijkstra:dijkstra('E200', 1, ud),
Ende7 = erlang:timestamp(),
Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
io:fwrite("~p ~n", [Diffms7]),

Start8 = erlang:timestamp(),
dijkstra:dijkstra('E225', 1, ud),
Ende8 = erlang:timestamp(),
Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
io:fwrite("~p ~n", [Diffms8]),

Start9 = erlang:timestamp(),
dijkstra:dijkstra('E250', 1, ud),
Ende9 = erlang:timestamp(),
Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
io:fwrite("~p ~n", [Diffms9]).


%% bellmanford directed
measurebellmannfordD() ->

Start = erlang:timestamp(),
bellmannford:bellmannford('E25', 1, d),
Ende = erlang:timestamp(),
Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
io:fwrite("~p ~n", [Diffms]),

Start1 = erlang:timestamp(),
bellmannford:bellmannford('E50', 1, d),
Ende1 = erlang:timestamp(),
Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
io:fwrite("~p ~n", [Diffms1]),

Start2 = erlang:timestamp(),
bellmannford:bellmannford('E75', 1, d),
Ende2 = erlang:timestamp(),
Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
io:fwrite("~p ~n", [Diffms2]),

Start3 = erlang:timestamp(),
bellmannford:bellmannford('E100', 1, d),
Ende3 = erlang:timestamp(),
Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
io:fwrite("~p ~n", [Diffms3]),

Start4 = erlang:timestamp(),
bellmannford:bellmannford('E125', 1, d),
Ende4 = erlang:timestamp(),
Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
io:fwrite("~p ~n", [Diffms4]),

Start5 = erlang:timestamp(),
bellmannford:bellmannford('E150', 1, d),
Ende5 = erlang:timestamp(),
Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
io:fwrite("~p ~n", [Diffms5]),

Start6 = erlang:timestamp(),
bellmannford:bellmannford('E175', 1, d),
Ende6 = erlang:timestamp(),
Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
io:fwrite("~p ~n", [Diffms6]),

Start7 = erlang:timestamp(),
bellmannford:bellmannford('E200', 1, d),
Ende7 = erlang:timestamp(),
Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
io:fwrite("~p ~n", [Diffms7]),

Start8 = erlang:timestamp(),
bellmannford:bellmannford('E225', 1, d),
Ende8 = erlang:timestamp(),
Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
io:fwrite("~p ~n", [Diffms8]),

Start9 = erlang:timestamp(),
bellmannford:bellmannford('E250', 1, d),
Ende9 = erlang:timestamp(),
Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
io:fwrite("~p ~n", [Diffms9]).


%% undirected
measurebellmannfordUD() ->
Start = erlang:timestamp(),
bellmannford:bellmannford('E25', 1, ud),
Ende = erlang:timestamp(),
Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
io:fwrite("~p ~n", [Diffms]),

Start1 = erlang:timestamp(),
bellmannford:bellmannford('E50', 1, ud),
Ende1 = erlang:timestamp(),
Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
io:fwrite("~p ~n", [Diffms1]),

Start2 = erlang:timestamp(),
bellmannford:bellmannford('E75', 1, ud),
Ende2 = erlang:timestamp(),
Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
io:fwrite("~p ~n", [Diffms2]),

Start3 = erlang:timestamp(),
bellmannford:bellmannford('E100', 1, ud),
Ende3 = erlang:timestamp(),
Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
io:fwrite("~p ~n", [Diffms3]),

Start4 = erlang:timestamp(),
bellmannford:bellmannford('E125', 1, ud),
Ende4 = erlang:timestamp(),
Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
io:fwrite("~p ~n", [Diffms4]),

Start5 = erlang:timestamp(),
bellmannford:bellmannford('E150', 1, ud),
Ende5 = erlang:timestamp(),
Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
io:fwrite("~p ~n", [Diffms5]),

Start6 = erlang:timestamp(),
bellmannford:bellmannford('E175', 1, ud),
Ende6 = erlang:timestamp(),
Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
io:fwrite("~p ~n", [Diffms6]),

Start7 = erlang:timestamp(),
bellmannford:bellmannford('E200', 1, ud),
Ende7 = erlang:timestamp(),
Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
io:fwrite("~p ~n", [Diffms7]),

Start8 = erlang:timestamp(),
bellmannford:bellmannford('E225', 1, ud),
Ende8 = erlang:timestamp(),
Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
io:fwrite("~p ~n", [Diffms8]),

Start9 = erlang:timestamp(),
bellmannford:bellmannford('E250', 1, ud),
Ende9 = erlang:timestamp(),
Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
io:fwrite("~p ~n", [Diffms9]).
