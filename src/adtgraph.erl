%%%-------------------------------------------------------------------
%%% @author Andre Soblechero
%%% @author Eike Meyer
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 27. Mär 2018 18:59
%%%-------------------------------------------------------------------
-module(adtgraph).
-author("Andre Soblechero").
-author("Eike Meyer").

%% API
-export([initBT/0, insertBT/2, deleteBT/2, createG/1, addVertex/2, deleteVertex/2, deleteEdge/3, addEdge/3, setAtV/4, getValE/4, getValV/3, getIncident/2, getAdjacent/2, getTarget/2, getSource/2, getEdges/1, getVertices/1, importG/2, exportG/2, printG/2, setAtE/5, benchmark/3, printGFF/2]).

%%definition of an edge in a Graph:
%%{Elem, {Attributename,AttributeValue,[{Neightbour1,Attributename,Attributevalue},....,{NeightbourN,Attributename,Attributevalue}]}, emptytree, emptytree}


%%
%% @doc Create an empty binary tree.
%% Hilfsfunktionen zum Erstellen und Bearbeiten eines Binärbaums
%% Entwurf: s.5 "Aufbau der Datenstruktur Graph"
initBT() ->
  emptytree.


%%
%% @doc Return a given tree's smallest value.
%%



get_min_value({Value, WeightTupel, emptytree, emptytree}) ->
  {Value, WeightTupel};
get_min_value({Value, WeightTupel, emptytree, _RightTree}) ->
  {Value, WeightTupel};
get_min_value({_Value, _Height, LeftTree, emptytree}) ->
  get_min_value(LeftTree);
get_min_value({_Value, _Height, LeftTree, _RightTree}) ->
  get_min_value(LeftTree).

%% @doc {Elem, {weight,AttributeValue,[{Neightbour1,Attributename,Attributevalue},....,{NeightbourN,Attributename,Attributevalue}]}, emptytree, emptytree}


%%
%% @doc Insert an element into the given tree at the correct position.
%%
% [Step 3]: There's no lower tree, reached the position to insert at.

insertBT(emptytree, Elem) ->
  {Elem, {weight, nil, []}, emptytree, emptytree};

insertBT({Elem, TreeAttribute, LeftTree, RightTree}, Elem) ->
  {Elem, TreeAttribute, LeftTree, RightTree};

%%
%% @doc Insert an element on the <i>left</i> side.
%%
% [Step 2]: Either enter the left or the right tree based on the element's value.
insertBT({Value, TreeAttribute, LeftTree, RightTree}, Elem) when Elem < Value ->
  LeftTreeInserted = insertBT(LeftTree, Elem),
  {Value, TreeAttribute, LeftTreeInserted, RightTree};
%%
%% @doc Insert an element on the <i>right</i> side.

%%
insertBT({Value, TreeAttribute, LeftTree, RightTree}, Elem) when Elem > Value ->
  RightTreeInserted = insertBT(RightTree, Elem),
  {Value, TreeAttribute, LeftTree, RightTreeInserted}.


deleteBT(emptytree, _Elem) ->
  emptytree;

deleteBT({Elem, _TreeAttribute, emptytree, emptytree}, Elem) ->
  emptytree;

deleteBT({Elem, _TreeAttribute, LeftTree, emptytree}, Elem) ->
  LeftTree;

deleteBT({Elem, _TreeAttribute, emptytree, RightTree}, Elem) ->
  RightTree;

deleteBT({Value, _TreeAttribute, LeftTree, RightTree}, Elem) when Value == Elem ->
  {Value, WeightTupel} = get_min_value(RightTree),
  NewRightTree = deleteBT(RightTree, Value),
  {Value, WeightTupel, LeftTree, NewRightTree};

deleteBT({Value, TreeAttribute, LeftTree, RightTree}, Elem) when Elem < Value ->
  {Value, TreeAttribute, deleteBT(LeftTree, Elem), RightTree};

deleteBT({Value, TreeAttribute, LeftTree, RightTree}, Elem) when Elem > Value ->
  {Value, TreeAttribute, LeftTree, deleteBT(RightTree, Elem)}.

%% Ende der Binaerbaum Hilfsfunktionen, Beginn der geforderten Funktionalitaeten


createG(ud) ->
  util:setglobalvar(yndirected, ud),
  initBT();
createG(d) ->
  util:setglobalvar(yndirected, d),
  initBT();
createG(_) ->
  nil.



addVertex(Tree, Vertex) ->
  insertBT(Tree, Vertex).

deleteVertex(Tree, Vertex) ->
  deleteVertexFromAdjList(deleteBT(Tree, Vertex),Vertex).


deleteVertexFromAdjList(emptytree,_)->emptytree;

deleteVertexFromAdjList({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex)->
  {Value, {Attribute, AttributeValue, deleteFromList(Adjazenzliste, Vertex)}, deleteVertexFromAdjList(LeftTree,Vertex), deleteVertexFromAdjList(RightTree,Vertex)}.


%%aaaadddddd
%%Bug-> Vertex is not deleted from the Adjazenzlist of other Vertex.

%%deleteFromList(List,Vertex)



%% @doc {Elem, {weight,AttributeValue,[{Neightbour1,Attributename,Attributevalue},....,{NeightbourN,Attributename,Attributevalue}]}, emptytree, emptytree}


addEdge(Tree, Vertex1, Vertex2) ->
  Yndirected= util:getglobalvar(yndirected),
  case Yndirected == d of
    true -> First=addEdge_directed(Tree, Vertex1, Vertex2),
            addVertex(First,Vertex2);
    _Else -> addEdge_undirected(Tree, Vertex1, Vertex2)
  end.

%%Vertex1 im Baum gefunden
addEdge_directed({Value, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Vertex1 == Value ->
  case edgeExists(Adjazenzliste, Vertex2) of
    true -> {Value, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree};
    _Else -> {Value, {_Attribute, _AttributeValue, [{Vertex2, weight, []}] ++ Adjazenzliste}, LeftTree, RightTree}
  end;

%%Vertex1 im Baum nicht gefunden -> Führe ein Add aus.
addEdge_directed(emptytree, Vertex1, Vertex2) ->
  {Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree} = addVertex(emptytree, Vertex1),
  {Value, {Attribute, AttributeValue, [{Vertex2, weight, []}] ++ Adjazenzliste}, LeftTree, RightTree};


%%Vertex1 im Baum suchen
addEdge_directed({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Value < Vertex1 ->
  {Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, addEdge_directed(RightTree, Vertex1, Vertex2)};

%%Vertex1 im Baum suchen
addEdge_directed({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Value > Vertex1 ->
  {Value, {Attribute, AttributeValue, Adjazenzliste}, addEdge_directed(LeftTree, Vertex1, Vertex2), RightTree}.


addEdge_undirected(Tree, Vertex1, Vertex2) ->
  NewTree = addEdge_directed(Tree, Vertex1, Vertex2),
  addEdge_directed(NewTree, Vertex2, Vertex1).



deleteEdge(Tree, Vertex1, Vertex2) ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true -> deleteEdge_directed(Tree, Vertex1, Vertex2);
    _Else -> deleteEdge_undirected(Tree, Vertex1, Vertex2)
  end.

deleteFromList([{A, _Attribute, _Value} | Rest], Elem) when A == Elem -> Rest;

deleteFromList([], _Elem) -> [];

deleteFromList([{A, Attribute, Value} | Rest], Elem) -> [{A, Attribute, Value}] ++ deleteFromList(Rest, Elem).



deleteEdge_directed({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Vertex1 == Value ->
  NewAdjazenzliste = deleteFromList(Adjazenzliste, Vertex2),
  {Value, {Attribute, AttributeValue, NewAdjazenzliste}, LeftTree, RightTree};


deleteEdge_directed(emptytree, _Vertex1, _Vertex2) -> emptytree;

deleteEdge_directed({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Vertex1 > Value ->
  {Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, deleteEdge_directed(RightTree, Vertex1, Vertex2)};

deleteEdge_directed({Value, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2) when Vertex1 < Value ->
  {Value, {Attribute, AttributeValue, Adjazenzliste}, deleteEdge_directed(LeftTree, Vertex1, Vertex2), RightTree}.


deleteEdge_undirected(Tree, Vertex1, Vertex2) ->
  NewTree = deleteEdge_directed(Tree, Vertex1, Vertex2),
  deleteEdge_directed(NewTree, Vertex2, Vertex1).

%% @doc Funktion um in einem Graphen die Attribute einer Kante zu setzen

setAtE(Tree, Vertex1, Vertex2, Name, Value) ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true -> setAtE_directed(Tree, Vertex1, Vertex2, Name, Value);
    _Else -> setAtE_undirected(Tree, Vertex1, Vertex2, Name, Value)
  end.


%% @doc  Hilfsfunktion um in der Adjazentsliste den AttributeNamen und Wert zu ändern eines bestimmten Vertex


setAtEAttributeList([],Name,Value)->[{Name,Value}];

setAtEAttributeList([{Name,_}|_Rest],Name,Value)->[{Name,Value}];

setAtEAttributeList([{Name1,Value2}|Rest],Name,Value)->[{Name1,Value2}|setAtEAttributeList(Rest,Name,Value)].



setAtEList([], _Elem, _Name, _Value) ->
  [];

setAtEList([{A, _Namem, ActualValue} | Rest], Elem, Name, Value) when A == Elem ->
  [{A, Name, setAtEAttributeList(ActualValue, Name, Value)} | Rest];

setAtEList([{ID, Attribute, Value} | Rest], Elem, Name, NewValue) ->
  [{ID, Attribute, Value}] ++ setAtEList(Rest, Elem, Name, NewValue).


%% @doc Funktion um in einem Graphen die Attribute einer Kante zu setzen

setAtE_directed({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2, Name, Value) when ID == Vertex1 ->
  {ID, {Attribute, AttributeValue, setAtEList(Adjazenzliste, Vertex2, Name, Value)}, LeftTree, RightTree};

setAtE_directed(empytree, _Vertex1, _Vertex2, _Name, _Value) -> emptytree;

setAtE_directed({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2, Name, Value) when ID < Vertex1 ->
  {ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, setAtE_directed(RightTree, Vertex1, Vertex2, Name, Value)};

setAtE_directed({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Vertex2, Name, Value) when ID > Vertex1 ->
  {ID, {Attribute, AttributeValue, Adjazenzliste}, setAtE_directed(LeftTree, Vertex1, Vertex2, Name, Value), RightTree}.

setAtE_undirected(Tree, Vertex1, Vertex2, Name, Value) ->
  NewTree = setAtE_directed(Tree, Vertex1, Vertex2, Name, Value),
  setAtE_directed(NewTree, Vertex2, Vertex1, Name, Value).


%% @doc Setzt den Attributenamen und Wert einer Kante im Graphen

setAtV(emptytree, _Vertex1, _Name, _Value) -> emptytree;

setAtV({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Name, Value) when ID == Vertex1 ->
  {ID, {Name, Value, Adjazenzliste}, LeftTree, RightTree};

setAtV({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Name, Value) when ID < Vertex1 ->
  {ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, setAtV(RightTree, Vertex1, Name, Value)};

setAtV({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex1, Name, Value) when ID > Vertex1 ->
  {ID, {Attribute, AttributeValue, Adjazenzliste}, setAtV(LeftTree, Vertex1, Name, Value), RightTree}.


%%getValEList -> Elem of Edge list is a tupel of {ID, Name, Value}. Will be changed to {ID, Name,[{Name, Value}|Rest]}

%% @doc Funktion den Wert einer Kante mit einem bestimmten Namen zu bekommen aus der Adjazenzliste

getValFromAttributeList([{Name, Value}|_Rest],Name)->Value;

getValFromAttributeList([],_Name)->nil;

getValFromAttributeList([{_Name1, _Value}|Rest],Name2)->getValFromAttributeList(Rest,Name2).




getValEList([{ID, _Name2, Value} | _Rest], Vertex, Name) when ID == Vertex -> getValFromAttributeList(Value,Name);

getValEList([{_ID, _AttributeName, _Value} | Rest], Vertex, Name) -> getValEList(Rest, Vertex, Name);

getValEList([], _Vertex, _Name) -> nil;

getValEList(_, _Vertex, _Name) -> nil.




%% @doc Funktion den Wert einer Kante mit einem bestimmten Namen zu bekommen

getValE(emptytree, _Vertex1, _Vertex2, _Name) ->
  nil;

getValE({ID, {_Attribute, _AttributeValue, Adjazenzliste}, _LeftTree, _RightTree}, Vertex1, Vertex2, Name) when ID == Vertex1 ->
  getValEList(Adjazenzliste, Vertex2, Name);

getValE({ID, {_Attribute, _AttributeValue, _}, _LeftTree, RightTree}, Vertex1, Vertex2, Name) when ID < Vertex1 ->
  getValE(RightTree, Vertex1, Vertex2, Name);

getValE({ID, {_Attribute, _AttributeValue, _}, LeftTree, _RightTree}, Vertex1, Vertex2, Name) when ID > Vertex1 ->
  getValE(LeftTree, Vertex1, Vertex2, Name).


%%Algorithmus:
%%1. Suche den entgegengenommenen Knoten „vertex1“
%%2. Vergleiche den entgegengenommenen Attributsnamen „name“ mit dem Attributsnamen des Knotens
%%3. Falls der AttributeName identisch zum eingegebenen „name“ ist, gib den zugehörigen Wert zurück, ansonsten „nil“

getValV(emptytree, _Vertex, _Name) -> nil;

getValV({ID, {Attribute, AttributeValue, _Adjazenzliste}, _LeftTree, _RightTree}, Vertex, Attribute) when ID == Vertex ->
  AttributeValue;

getValV({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, _LeftTree, RightTree}, Vertex, Name) when ID < Vertex ->
  getValV(RightTree, Vertex, Name);

getValV({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, LeftTree, _RightTree}, Vertex, Name) when ID > Vertex ->
  getValV(LeftTree, Vertex, Name);

getValV(_, _Vertex, _Name) -> nil.


%% @doc Hilfsfunktion zur erstellung der vertexliste
%%  ALLE KANTEN DIE VON EINEM KNOTEN AUSGEHEN

makeIncidentList([], _ID, NewList) -> NewList;

makeIncidentList([{ID2, _Name, _Value} | Rest], ID, NewList) -> makeIncidentList(Rest, ID, [ID, ID2 | NewList]).


getIncident(emptytree, _Vertex) ->
  nil;

getIncident({ID, {_Attribute, _AttributeValue, Adjazenzliste}, _LeftTree, _RightTree}, Vertex) when ID == Vertex ->
  makeIncidentList(Adjazenzliste, Vertex, []);

getIncident({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, _LeftTree, RightTree}, Vertex) when ID < Vertex ->
  getIncident(RightTree, Vertex);

getIncident({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, LeftTree, _RightTree}, Vertex) when ID > Vertex ->
  getIncident(LeftTree, Vertex).


makeAdjacentList([], NewList) -> NewList;

makeAdjacentList([{ID, _Name, _Value} | Rest], NewList) -> makeAdjacentList(Rest, [ID | NewList]).

%%  GIBT ALLE KNOTEN ZURÜCK, DIE VOM JWEILIGEN KNOTEN ERREICHT oder bei gerichteten Graphen als Source dienen zurück
%%

getAdjacent_directed(emptytree, _, _) -> nil;

getAdjacent_directed({ID, {_Attribute, _AttributeValue, Adjazenzliste}, _LeftTree, _RightTree}, Vertex, SourceTree) when ID == Vertex ->
  Sources = getSource(SourceTree, ID),
  %io:fwrite("Source of Vertex: ~p~n",[Sources]),
  VBildList=makeAdjacentList(Adjazenzliste, []),
  %io:fwrite("Targets of Vertex: ~p~n",[VBildList]),
  Sources ++ VBildList;

%%Kombination aus getTarget und getSource

getAdjacent_directed({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, _LeftTree, RightTree}, Vertex, SourceTree) when ID < Vertex ->
  getAdjacent_directed(RightTree, Vertex, SourceTree);

getAdjacent_directed({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, LeftTree, _RightTree}, Vertex, SourceTree) when ID > Vertex ->
  getAdjacent_directed(LeftTree, Vertex, SourceTree).



getAdjacent(emptytree, _Vertex) ->
  nil;

getAdjacent({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex) when ID == Vertex ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true -> Tree = {ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree},
             getAdjacent_directed(Tree, Vertex, Tree);
    _Else -> makeAdjacentList(Adjazenzliste, [])
  end;



getAdjacent({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex) when ID < Vertex ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true -> Tree = {ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree},
             getAdjacent_directed(Tree, Vertex,Tree);
    _Else -> getAdjacent(RightTree, Vertex)
  end;

getAdjacent({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex) when ID > Vertex ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true ->  Tree={ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree},
      getAdjacent_directed(Tree, Vertex, Tree);
    _Else -> getAdjacent(LeftTree, Vertex)
  end.



getTarget(emptytree, _Vertex) ->
  nil;

getTarget({ID, {_Attribute, _AttributeValue, Adjazenzliste}, _LeftTree, _RightTree}, Vertex) when ID == Vertex ->
  makeAdjacentList(Adjazenzliste, []);

getTarget({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, _LeftTree, RightTree}, Vertex) when ID < Vertex ->
  getTarget(RightTree, Vertex);

getTarget({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, LeftTree, _RightTree}, Vertex) when ID > Vertex ->
  getTarget(LeftTree, Vertex).



edgeExists([], _Vertex) ->
  false;
edgeExists([{ID, _Name, _Value} | Rest], Vertex) when ID =/= Vertex ->
  edgeExists(Rest, Vertex);
edgeExists([{Vertex, _Name, _Value} | _Rest], Vertex) ->
  true.


%%Returns a List of IDs which has edges going to a given Vertex
getSource(emptytree, _Vertex) ->
  [];

getSource({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}, Vertex) ->
  case edgeExists(Adjazenzliste, Vertex) of
    true -> [ID] ++ getSource(LeftTree, Vertex) ++ getSource(RightTree, Vertex);
    _Else -> getSource(LeftTree, Vertex) ++ getSource(RightTree, Vertex)
  end.

%%NewList ist dazu da um zu überprüfen ob es diese Kante bereits in der Liste gibt


checkIfEdgeExists(_Vertex1, _Vertex2, []) -> false;

checkIfEdgeExists(ID1, ID, [ID1, ID | _Rest]) -> true;

checkIfEdgeExists(ID1, ID, [ID, ID1 | _Rest]) -> true;

checkIfEdgeExists(ID1, ID, [_A, _B | Rest]) -> checkIfEdgeExists(ID1, ID, Rest).


addToList([], NewList) -> NewList;

addToList([ID1, ID2 | Rest], NewList) ->
  case checkIfEdgeExists(ID1, ID2, NewList) of
    true -> addToList(Rest, NewList);
    _Else -> addToList(Rest, [ID1, ID2 | NewList])
  end.

getEdges_undirected(emptytree) -> [];

getEdges_undirected({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}) ->
  getEdges_undirected(LeftTree) ++ makeIncidentList(Adjazenzliste, ID, [])++ getEdges_undirected(RightTree).


getEdges(emptytree) -> [];

getEdges({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}) ->
  Yndirected = util:getglobalvar(yndirected),
  case Yndirected == d of
    true ->  getEdges(LeftTree) ++ makeIncidentList(Adjazenzliste, ID, [])++ getEdges(RightTree);
    _Else -> RudundantList= getEdges_undirected({ID, {Attribute, AttributeValue, Adjazenzliste}, LeftTree, RightTree}),
              addToList(RudundantList,[])
  end.


getVertices(emptytree) -> [];

getVertices({ID, {_Attribute, _AttributeValue, _Adjazenzliste}, LeftTree, RightTree}) ->
  [ID] ++ getVertices(LeftTree) ++ getVertices(RightTree).


read(emptytree, IoDev) ->
  {ok, [Vertex1, Vertex2, Attribute]} = io:fread(IoDev, [], "[~d,~d,~d"),
  %io:fwrite("[~p,~p,~p,", [Vertex1, Vertex2, Attribute]),
  NewTree = addEdge(emptytree, Vertex1, Vertex2),
  NewNewTree = setAtE(NewTree, Vertex1, Vertex2, weight, Attribute),
  read(NewNewTree, IoDev);

read(Tree, IoDev) ->
  case io:fread(IoDev, [], ",~d,~d,~d") of
    {ok, [Vertex1, Vertex2, Attribute]} ->
      %io:fwrite("~p,~p,~p,", [Vertex1, Vertex2, Attribute]),
      NewTree = addEdge(Tree, Vertex1, Vertex2),
      NewNewTree = setAtE(NewTree, Vertex1, Vertex2, weight, Attribute),
      read(NewNewTree, IoDev);
    eof ->
      %ite("Reading finished"),
      Tree;
    {_X, _What} ->
      Tree
  end.


importG(Filename1, d) ->

  Path2 = util:attachEnding(Filename1, 'graph'),
  Tree = createG(d),
  %io:fwrite("Import Directed: ~p~n", [Path2]),
  {ok, Device} = file:open(Path2, [read]),
  NewTree = read(Tree, Device),
  file:close(Device),
  NewTree;

importG(Filename1, ud) ->
  Path2 = util:attachEnding(Filename1, 'graph'),
  Tree = createG(ud),
  %io:fwrite("Import Undirected: ~p~n", [Path2]),
  {ok, Device} = file:open(Path2, [read]),
  NewTree = read(Tree, Device),
  file:close(Device),
  NewTree
.


makeExportPartList([], _ID, NewList) -> NewList;

makeExportPartList([{ID2, _Name, Value} | Rest], ID, NewList) ->
  makeExportPartList(Rest, ID, [ID, ID2, Value | NewList]).


getListToExport(emptytree) -> [];

getListToExport({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}) ->
  makeExportPartList(Adjazenzliste, ID, []) ++ getListToExport(LeftTree) ++ getListToExport(RightTree).


exportG(Tree, Filename1) ->
  Path = util:attachEnding(Filename1, 'graph'),
  io:fwrite("Start exporting~n"),
  {ok, Device} = file:open(Path,     [write]),
  io:fwrite(Device, "~p", [getListToExport(Tree)]),
  file:close(Device),
  ok.


%%
%% @doc Output the given Graph tree as a Graphviz-compatible dot file.
%%
printG(emptytree, _) ->
  io:fwrite("Trying to draw an empty tree, which isn't possible.");
printG(Tree, Filename) ->
  DotFile = io_lib:format("~s.dot", [Filename]),
  {ok, IODevice} = file:open(DotFile, [write]),
  io:fwrite(IODevice, "digraph ~s {~n", [Filename]),
  io:fwrite(IODevice, generate_dot(Tree), []),
  io:fwrite(IODevice, "}", []),
file:close(IODevice),

dot_to_svg(io_lib:format("~s.dot", [Filename])).




makeVertexPrintAbleGFF([], _ID, String) -> String;

makeVertexPrintAbleGFF([{ID2, _Name, ValueList} | Rest], ID, String) ->
  makeVertexPrintAbleGFF(Rest, ID, String ++ lists:flatten(io_lib:format("~p -> ~p [headlabel=~p, taillabel=~p];~n", [ID, ID2,getValFromAttributeList(ValueList,weight),getValFromAttributeList(ValueList,flow)]))).
%% @doc Capacität is head of edge and flow is tail of edge

generate_dotGFF(emptytree) -> "";

generate_dotGFF({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}) ->
  makeVertexPrintAbleGFF(Adjazenzliste, ID, "") ++ generate_dotGFF(LeftTree) ++ generate_dotGFF(RightTree).


%% @doc Capacität is head of edge and flow is tail of edge
printGFF(emptytree, _) ->
  io:fwrite("Trying to draw an empty tree, which isn't possible.");
printGFF(Tree, Filename) ->
  DotFile = io_lib:format("~s.dot", [Filename]),
  {ok, IODevice} = file:open(DotFile, [write]),
  io:fwrite(IODevice, "digraph ~s {~n", [Filename]),
  io:fwrite(IODevice, generate_dotGFF(Tree), []),
  io:fwrite(IODevice, "}", []),
  file:close(IODevice),

  dot_to_svg(io_lib:format("~s.dot", [Filename])).
%% @doc Capacität is head of edge and flow is tail of edge



%%
%% @doc Generate a tree's inner dot file contents (i.e. everything between the {}).
%%


makeVertexPrintAble([], _ID, String) -> String;

makeVertexPrintAble([{ID2, _Name, [{_Name,Value}|Rest]} | Rest], ID, String) ->
  makeVertexPrintAble(Rest, ID, String ++ lists:flatten(io_lib:format("~p -> ~p [label=~p];~n", [ID, ID2, Value]))).


generate_dot(emptytree) -> "";

generate_dot({ID, {_Attribute, _AttributeValue, Adjazenzliste}, LeftTree, RightTree}) ->
  makeVertexPrintAble(Adjazenzliste, ID, "") ++ generate_dot(LeftTree) ++ generate_dot(RightTree).



dot_to_svg(DotFile) ->
  Command = io_lib:format("dot -Tsvg ~s > ~s-graph.svg", [DotFile, DotFile]),
  os:cmd(Command).


generateTree(Tree, []) -> Tree;

generateTree(Tree, [A, B | Rest]) ->
  NewTree = addEdge(Tree, A, B),
  generateTree(NewTree, Rest).



%%
%%
%%Funktion fäng bei der Anzahl "Adder" -Knoten/2 an 
%%Wird in 50. Schritten hochgezählt, so oft wie Index angegeben ist.
%%->benchmark(3000,d,100)
%% Knoten von 3000-8000, weil 100*50=5000 und 5000+3000=8000


benchmark_run(Index, DUD, IO,Adder) when Index> 0 ->
  %%PREPARE
  A=Adder+200,
  Tree = createG(DUD),
  RandList = util:randomliste(A,0,1000), %%%Liste mit
  NewTree = generateTree(Tree, RandList),
  RandElemGT=lists:nth(rand:uniform(length(RandList)),RandList),

  %%RUN BENCHMARK
  {GetTargetTime,_}=timer:tc(adtgraph,getTarget,[NewTree,RandElemGT]),
  {GetEdgesTime,[A1,B1|_List]}=timer:tc(adtgraph,getEdges,[NewTree]),
  {SetAtElemTime,_}=timer:tc(adtgraph,setAtE,[NewTree,A1,B1,weight,10]),

  %%WRITE TO .csv
  io:fwrite(IO,"~p,~p,~p,~p~n",[A/2,GetTargetTime,GetEdgesTime,SetAtElemTime]),
  benchmark_run(Index-1,DUD,IO,A);

benchmark_run(_,_,_,_)->ok.


%%Funktion fäng bei der Anzahl "Adder" -Knoten/2 an 
%%Wird in 50. Schritten hochgezählt, so oft wie Index angegeben ist.
%%->benchmark(3000,d,100)
%% Knoten von 3000-8000, weil 100*50=5000 und 5000+3000=8000

benchmark(Adder, DUD,Index)->
  {ok, ResultsFile} = file:open("results.csv", [write]),
  io:fwrite(ResultsFile, "|Edges|,getTarget,getEdges,setAtE~n", []),
  benchmark_run(Index, DUD, ResultsFile, Adder),
  file:close(ResultsFile).
