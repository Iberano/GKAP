%%%-------------------------------------------------------------------
%%% @author Andre Soblechero
%%% @author Eike Meyer
%%% @doc
%%%
%%% @end
%%% Created : 27. Mär 2018 20:43
%%%-------------------------------------------------------------------
-module(adtgraph_tests).
-author("Andre Soblechero").
-author("Eike Meyer").

-include_lib("eunit/include/eunit.hrl").


test_test() ->
  graph0_test(),
  graph1_test(),
  graph2_test(),
  graph3_test(),
  graph4_test(),
  graph5_test(),
  graph6_test(),
  graph7_test(),
  adjacencySourceTarget_test(),
  printG_test(),
  graph8_test(),
  getEdgesUD_test(),
  getAdjacentD_test(),
  getAdjacentDImport_test(),
  getSourceDImport_test(),
  getSourceD_test(),
  adjacencySourceTarget_test(),
  adjacencySourceTargetMS_test(),
  graph11_test(),
  graph12_test(),
  setGetAtE_test(),
  setGetAtV_test().

%% exakt ein Knoten wird eingefügt
graph0_test() ->
    Start = erlang:timestamp(),
    G = adtgraph:createG(d),

    E1 = adtgraph:addVertex(G,1),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),

    io:fwrite("~p", [E1]).


%% addEdge wird auf einem leeren Graphen aufgerufen
graph1_test() ->
    Start = erlang:timestamp(),
    A=adtgraph:createG(ud),

    V3=adtgraph:addEdge(A,1,2),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),
    io:fwrite("~p", [V3]).


%% vollständiger, ungerichteter K4
graph2_test() ->
    Start = erlang:timestamp(),
    B=adtgraph:createG(ud),

    V1=adtgraph:addEdge(B,33,64),
    V2=adtgraph:addEdge(V1,33,5),
    V3=adtgraph:addEdge(V2,33,21),
    V4=adtgraph:addEdge(V3,64,5),
    V5=adtgraph:addEdge(V4,64,21),
    V6=adtgraph:addEdge(V5,21,5),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),
    io:fwrite("~p", [V6]).

%% gerichteter K3
graph3_test() ->
    Start = erlang:timestamp(),
    G = adtgraph:createG(d),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,2,3),
    V3 = adtgraph:addEdge(V2,3,1),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),

    io:fwrite("~p", [V3]).


%% gleiche Kante mehrfach hinzugefügt, undirected
graph4_test() ->
    Start = erlang:timestamp(),
    G = adtgraph:createG(ud),

    E1 = adtgraph:addEdge(G,1,2),
    E2 = adtgraph:addEdge(E1,1,2),
    E3 = adtgraph:addEdge(E2,1,2),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),

    io:fwrite("~p", [E3]).


%% gleiche Kante mehrfach hinzugefügt, directed
graph5_test() ->
    Start = erlang:timestamp(),
    G = adtgraph:createG(d),

    E1 = adtgraph:addEdge(G,1,2),
    E2 = adtgraph:addEdge(E1,1,2),
    E3 = adtgraph:addEdge(E2,1,2),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),

    io:fwrite("~p", [E3]).


%% Nicht vorhandene Kante gelöscht
graph6_test() ->
    Start = erlang:timestamp(),
    G = adtgraph:createG(d),

    E1 = adtgraph:addEdge(G,1,2),

    E0 = adtgraph:deleteEdge(E1,1,2),

    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p \n", [Diffms]),

    io:fwrite("~p", [E0]).

%% falscher createG parameter
graph7_test() ->
    adtgraph:createG(ff).


printG_test()->
    A=adtgraph:importG('graph_06',d),
    adtgraph:printG(A,'Testgraph').


%% create G test, löschen von Kanten
graph8_test() ->
    G = adtgraph:createG(d),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,2,3),
    V3 = adtgraph:addEdge(V2,3,1),

    V4 = adtgraph:deleteEdge(V3,1,2),

    adtgraph:printG(V4,'Graph8_test').


%% target vs adjacency vs sources
adjacencySourceTarget_test() ->
    G = adtgraph:createG(d),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,3,4),
    V3 = adtgraph:addEdge(V2,1,3),

    ADJ = adtgraph:getAdjacent(V3,3),
    TGT = adtgraph:getTarget(V3,3),
    SRC = adtgraph:getSource(V3,3),

    io:fwrite("Adjazent ~p \n", [ADJ]),
    io:fwrite("Target ~p \n", [TGT]),
    io:fwrite("Source ~p \n", [SRC]),

    adtgraph:printG(V3,'Graph9_test').


 %% target vs adjacency, multible sources, targets
adjacencySourceTargetMS_test() ->
     G = adtgraph:createG(d),

     V1 = adtgraph:addEdge(G,1,2),
     V2 = adtgraph:addEdge(V1,3,4),
     V3 = adtgraph:addEdge(V2,1,3),
     V4 = adtgraph:addEdge(V3,5,3),
     V5 = adtgraph:addEdge(V4,3,6),

     ADJ = adtgraph:getAdjacent(V5,3),
     TGT = adtgraph:getTarget(V5,3),
     SRC = adtgraph:getSource(V5,3),

     io:fwrite("Adjazent ~p \n", [ADJ]),
     io:fwrite("Target ~p \n", [TGT]),
     io:fwrite("Source ~p \n", [SRC]),

     adtgraph:printG(V5,'Graph10_test').

%% inzidenz directet
graph11_test() ->
    G = adtgraph:createG(d),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,1,3),
    V3 = adtgraph:addEdge(V2,1,4),
    V4 = adtgraph:addEdge(V3,2,4),

%%    2---
%%    |  |
%%  3-1- 4
%%

    INZ = adtgraph:getIncident(V4,2),

    io:fwrite("Inzident ~p", [INZ]).


%% inzidenz undirectet
graph12_test() ->
    G = adtgraph:createG(ud),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,1,3),
    V3 = adtgraph:addEdge(V2,1,4),
    V4 = adtgraph:addEdge(V3,2,4),

    INZ = adtgraph:getIncident(V4,1),

    adtgraph:printG(V3,'Graph12_test'),

    io:fwrite("Inzident ~p", [INZ]).

%% setAtE / getValE, directed
setGetAtE_test() ->
    G = adtgraph:createG(d),

     ValE = 23,

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:setAtE(V1,1,2, gewicht, ValE),

    GewichtAtE12 = adtgraph:getValE(V2,1,2, gewicht),
    GewichtAtE21 = adtgraph:getValE(V2,2,1, gewicht),

    io:fwrite("Erwartetes Gewicht: ~p \n", [ValE]),
    io:fwrite("Gewicht an Kante 1,2 = ~p \n", [GewichtAtE12]),
    io:fwrite("Gewicht an Kante 2,1 = ~p \n", [GewichtAtE21]).

%% setAtV / getValV, directed
setGetAtV_test() ->
    G = adtgraph:createG(d),

    ValV = 32,

   V1 = adtgraph:addEdge(G,1,2),
   V2 = adtgraph:setAtV(V1,1,gewicht,ValV),

   GewichtAtV1 = adtgraph:getValV(V2,1,gewicht),
   GewichtAtV2 = adtgraph:getValV(V2,2,gewicht),

   io:fwrite("Gewicht an Knoten 1 = ~p \n", [GewichtAtV1]),
   io:fwrite("Gewicht an Knoten 2 = ~p \n", [GewichtAtV2]).

getEdgesUD_test() ->
    G = adtgraph:createG(ud),

     V1 = adtgraph:addEdge(G,1,2),
     V2 = adtgraph:addEdge(V1,2,3),
     V3 = adtgraph:addEdge(V2,3,4),
     V4 = adtgraph:addEdge(V3,4,1),
     V5 = adtgraph:addEdge(V4,3,5),

%%  1 - 2
%%  |   |
%%  4 - 3 - 5

     EdgesUD = adtgraph:getEdges(V5),

     io:fwrite("Kanten im Graphen ~p \n", [EdgesUD]).

getAdjacentD_test() ->
    G = adtgraph:createG(ud),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,2,3),
    V3 = adtgraph:addEdge(V2,3,4),
    V4 = adtgraph:addEdge(V3,4,1),
    V5 = adtgraph:addEdge(V4,3,5),

  %%  1 - 2
  %%  |   |
  %%  4 - 3 - 5

    ADJ = adtgraph:getAdjacent(V5,1),

    io:fwrite("Adjazente Knoten zu 1: ~p \n", [ADJ]).

getAdjacentDImport_test() ->
    G = adtgraph:importG('graph_06',d),
    adtgraph:printG(G, 'testGraph06'),

    ADJ = adtgraph:getAdjacent(G,88),

    io:fwrite("~p\n ", [G]),
    io:fwrite("Adjazente Knoten zu 88: ~p \n", [ADJ]).

getSourceDImport_test() ->
    G = adtgraph:importG('graph_06',d),
    adtgraph:printG(G, 'testGraph06'),

    ADJ = adtgraph:getSource(G,88),

    io:fwrite("~p\n ", [G]),
    io:fwrite("Quellen von Knoten 88: ~p \n", [ADJ]).

getSourceD_test() ->
    G = adtgraph:createG(d),

    V1 = adtgraph:addEdge(G,1,2),
    V2 = adtgraph:addEdge(V1,2,3),
    V3 = adtgraph:addEdge(V2,3,4),
    V4 = adtgraph:addEdge(V3,4,1),
    V5 = adtgraph:addEdge(V4,3,5),

    SRC = adtgraph:getSource(V5,3),

    io:fwrite("Quellen von Knoten 3: ~p \n", [SRC]).

    %%  1 - 2
    %%  |   |
    %%  4 - 3 - 5
    %% HIgh

zA_test() ->
      G = adtgraph:createG(ud),

        V1 = adtgraph:addEdge(G,1,2),

        %%io:fwrite("~p \n", [V1]),

        V2 = adtgraph:setAtE(V1,1,2,alter,1),
        V3 = adtgraph:setAtE(V2,1,2,name,2),
        V4 = adtgraph:setAtE(V3,1,2,test,3),

        io:fwrite("~p \n", [V4]),


        A1 = adtgraph:getValE(V4,1,2,alter),
        A2 = adtgraph:getValE(V4,1,2,name),
        A3 = adtgraph:getValE(V4,1,2,test),

        io:fwrite("~p \n", [A1]),
        io:fwrite("~p \n", [A2]),
        io:fwrite("~p \n", [A3]).

deleteVertex_test()->
  A=adtgraph:createG(d),
  A1=adtgraph:addEdge(A,1,2),
  A2=adtgraph:addEdge(A1,2,3),
  A3=adtgraph:addEdge(A2,3,4),
  A4=adtgraph:addEdge(A3,4,3),
  A5=adtgraph:addEdge(A4,4,1),
  A6=adtgraph:deleteVertex(A5,3),
io:fwrite("~p",[A6]).

printGFF_test() ->
    G = adtgraph:createG(ud),

            V1 = adtgraph:addEdge(G,1,2),

            V2 = adtgraph:setAtE(V1,1,2,weight,1),
            V3 = adtgraph:setAtE(V2,1,2,flow,2),

            adtgraph:printGFF(V3, zweiAttribute ).

zAK_test() ->
      G = adtgraph:createG(ud),

        V1 = adtgraph:addEdge(G,1,2),

        %%io:fwrite("~p \n", [V1]),

        V2 = adtgraph:setAtV(V1,1,alter,{3,4}),

        A1 = adtgraph:getValV(V2,1,alter),


        io:fwrite("~p \n", [A1]).
