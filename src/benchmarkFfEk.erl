-module(benchmarkFfEk).
-export([measureEdmondsKarp2/0, measureEdmondsKarp8/0]).



%% EdmondsKarp 2
measureEdmondsKarp2() ->

    Start = erlang:timestamp(),
    edmondskarp:edmondskarpT(graph_2_50, 29,47),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_100', 58, 72),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_150', 85, 95),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_200', 166, 198),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_250', 185, 243),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_300', 235, 280),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_350', 178, 313),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_400', 227, 308),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_450', 278, 315),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_2_500', 285, 437),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).



%% EdmondsKarp 8
measureEdmondsKarp8() ->

    Start = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_50', 14, 37),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_100', 70, 77),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_150', 8, 136),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_200', 47, 59),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_250', 233, 239),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_300', 13, 262),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_350', 112, 238),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_400', 306, 362),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_450', 32, 224),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    edmondskarp:edmondskarpT('graph_8_500', 54, 188),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]),

     Start9 = erlang:timestamp(),
     edmondskarp:edmondskarpT('graph_8_600', 160, 193),
     Ende9 = erlang:timestamp(),
     Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
     io:fwrite("~p ~n", [Diffms9]),

      Start9 = erlang:timestamp(),
      edmondskarp:edmondskarpT('graph_8_700', 153, 379),
      Ende9 = erlang:timestamp(),
      Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
      io:fwrite("~p ~n", [Diffms9]),

      Start9 = erlang:timestamp(),
      edmondskarp:edmondskarpT('graph_8_800', 294, 325),
      Ende9 = erlang:timestamp(),
      Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
      io:fwrite("~p ~n", [Diffms9]).


measureEdmondsKarpK() ->

