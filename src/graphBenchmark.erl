-module(graphBenchmark).
-author("Andre Soblechero").
-author("Eike Meyer").

-export([createDegree2Graphs/0, createDegree4Graphs/0, createDegree8Graphs/0, createDegree16Graphs/0,
measureBellmanFordLineD/0, measureBellmanFordLineUD/0, measureDijkstraLineD/0, measureDijkstraLineUD/0,
measureBellmanFord8D/0, measureBellmanFord8UD/0, measureDijkstra8D/0, measureDijkstra8UD/0, createDataPoints/0,
mbgBUD/0, mbgBD/0, mbgDUD/0, mbgDD/02
 ]).

createDegree2Graphs() ->
    gengraph:gengraph(50,2,2,1,1,'graph_2_50.graph'),
    gengraph:gengraph(100,2,2,1,1,'graph_2_100.graph'),
    gengraph:gengraph(150,2,2,1,1,'graph_2_150.graph'),
    gengraph:gengraph(200,2,2,1,1,'graph_2_200.graph'),
    gengraph:gengraph(250,2,2,1,1,'graph_2_250.graph'),
    gengraph:gengraph(300,2,2,1,1,'graph_2_300.graph'),
    gengraph:gengraph(350,2,2,1,1,'graph_2_350.graph'),
    gengraph:gengraph(400,2,2,1,1,'graph_2_400.graph'),
    gengraph:gengraph(450,2,2,1,1,'graph_2_450.graph'),
    gengraph:gengraph(500,2,2,1,1,'graph_2_500.graph').

createDegree4Graphs() ->
    gengraph:gengraph(50,4,4,1,1,'graph_4_50.graph'),
    gengraph:gengraph(100,4,4,1,1,'graph_4_100.graph'),
    gengraph:gengraph(150,4,4,1,1,'graph_4_150.graph'),
    gengraph:gengraph(200,4,4,1,1,'graph_4_200.graph'),
    gengraph:gengraph(250,4,4,1,1,'graph_4_250.graph'),
    gengraph:gengraph(300,4,4,1,1,'graph_4_300.graph'),
    gengraph:gengraph(350,4,4,1,1,'graph_4_350.graph'),
    gengraph:gengraph(400,4,4,1,1,'graph_4_400.graph'),
    gengraph:gengraph(450,4,4,1,1,'graph_4_450.graph'),
    gengraph:gengraph(500,4,4,1,1,'graph_4_500.graph').

createDegree8Graphs() ->
    gengraph:gengraph(50,8,8,1,1,'graph_8_50.graph'),
    gengraph:gengraph(100,8,8,1,1,'graph_8_100.graph'),
    gengraph:gengraph(150,8,8,1,1,'graph_8_150.graph'),
    gengraph:gengraph(200,8,8,1,1,'graph_8_200.graph'),
    gengraph:gengraph(250,8,8,1,1,'graph_8_250.graph'),
    gengraph:gengraph(300,8,8,1,1,'graph_8_300.graph'),
    gengraph:gengraph(350,8,8,1,1,'graph_8_350.graph'),
    gengraph:gengraph(400,8,8,1,1,'graph_8_400.graph'),
    gengraph:gengraph(450,8,8,1,1,'graph_8_450.graph'),
    gengraph:gengraph(500,8,8,1,1,'graph_8_500.graph').

createDegree16Graphs() ->
    gengraph:gengraph(50,16,16,1,1,'graph_8_50.graph'),
    gengraph:gengraph(100,16,16,1,1,'graph_8_100.graph'),
    gengraph:gengraph(150,16,16,1,1,'graph_8_150.graph'),
    gengraph:gengraph(200,16,16,1,1,'graph_8_200.graph'),
    gengraph:gengraph(250,16,16,1,1,'graph_8_250.graph'),
    gengraph:gengraph(300,16,16,1,1,'graph_8_300.graph'),
    gengraph:gengraph(350,16,16,1,1,'graph_8_350.graph'),
    gengraph:gengraph(400,16,16,1,1,'graph_8_400.graph'),
    gengraph:gengraph(450,16,16,1,1,'graph_8_450.graph'),
    gengraph:gengraph(500,16,16,1,1,'graph_8_500.graph').

createDataPoints() ->
    gengraph:gengraph(600,8,8,1,1,'graph_8_600.graph'),
    gengraph:gengraph(700,8,8,1,1,'graph_8_700.graph'),
    gengraph:gengraph(800,8,8,1,1,'graph_8_800.graph').

%% -------- Ende der Graphen Erstellung ----------

%% bellmanford directed
measureBellmanFordLineD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_50', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_100', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_150', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_200', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_250', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_300', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_350', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_400', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_450', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_500', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% bellmanford undirected
measureBellmanFordLineUD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_50', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_100', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_150', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_200', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_250', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_300', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_350', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_400', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_450', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('graph_2_500', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line directed
measureDijkstraLineD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_50', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_100', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_150', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_200', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_250', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_300', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_350', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_400', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_450', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_500', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line undirected
measureDijkstraLineUD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_50', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_100', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_150', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_200', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_250', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_300', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_350', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_400', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_450', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('graph_2_500', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% bellmanford directed
%% Bigger Graphs
measureBellmanFord8D() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_50', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_100', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_150', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_200', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_250', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_300', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_350', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_400', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_450', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_500', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% bellmanford undirected
measureBellmanFord8UD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_50', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_100', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_150', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_200', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_250', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_300', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_350', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_400', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_450', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_500', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line directed
measureDijkstra8D() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_50', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_100', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_150', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_200', 1, d),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_250', 1, d),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_300', 1, d),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_350', 1, d),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_400', 1, d),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_450', 1, d),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_500', 1, d),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% Dijkstra Line undirected
measureDijkstra8UD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_50', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_100', 1,ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_150', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]),

    Start3 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_200', 1, ud),
    Ende3 = erlang:timestamp(),
    Diffms3 = util:float_to_int(timer:now_diff(Ende3,Start3)/1000),
    io:fwrite("~p ~n", [Diffms3]),

    Start4 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_250', 1, ud),
    Ende4 = erlang:timestamp(),
    Diffms4 = util:float_to_int(timer:now_diff(Ende4,Start4)/1000),
    io:fwrite("~p ~n", [Diffms4]),

    Start5 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_300', 1, ud),
    Ende5 = erlang:timestamp(),
    Diffms5 = util:float_to_int(timer:now_diff(Ende5,Start5)/1000),
    io:fwrite("~p ~n", [Diffms5]),

    Start6 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_350', 1, ud),
    Ende6 = erlang:timestamp(),
    Diffms6 = util:float_to_int(timer:now_diff(Ende6,Start6)/1000),
    io:fwrite("~p ~n", [Diffms6]),

    Start7 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_400', 1, ud),
    Ende7 = erlang:timestamp(),
    Diffms7 = util:float_to_int(timer:now_diff(Ende7,Start7)/1000),
    io:fwrite("~p ~n", [Diffms7]),

    Start8 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_450', 1, ud),
    Ende8 = erlang:timestamp(),
    Diffms8 = util:float_to_int(timer:now_diff(Ende8,Start8)/1000),
    io:fwrite("~p ~n", [Diffms8]),

    Start9 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_500', 1, ud),
    Ende9 = erlang:timestamp(),
    Diffms9 = util:float_to_int(timer:now_diff(Ende9,Start9)/1000),
    io:fwrite("~p ~n", [Diffms9]).

%% bellmanford directed
mbgBD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_600', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_700', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_800', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]).

%% bellmanford undirected
mbgBUD() ->

    Start = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_600', 1, ud),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_700', 1, ud),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    bellmannford:bellmannford('graph_8_800', 1, ud),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]).

%% Dijkstra directed
mbgDD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_600', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_700', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_800', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]).

%% Dijkstra directed
mbgDUD() ->

    Start = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_600', 1, d),
    Ende = erlang:timestamp(),
    Diffms = util:float_to_int(timer:now_diff(Ende,Start)/1000),
    io:fwrite("~p ~n", [Diffms]),

    Start1 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_700', 1, d),
    Ende1 = erlang:timestamp(),
    Diffms1 = util:float_to_int(timer:now_diff(Ende1,Start1)/1000),
    io:fwrite("~p ~n", [Diffms1]),

    Start2 = erlang:timestamp(),
    dijkstra:dijkstra('graph_8_800', 1, d),
    Ende2 = erlang:timestamp(),
    Diffms2 = util:float_to_int(timer:now_diff(Ende2,Start2)/1000),
    io:fwrite("~p ~n", [Diffms2]).