-module(fordfulkerson).
-author ("Eike Meyer").

-export([fordfulkerson/3,fordfulkersonT/3]).

%% Implementiere Stack als Heuristik! Push / Pop

%% Hilfsfunktionen für 3.Inspektion und Markierung


initEdges(Graph, []) -> Graph;

initEdges(Graph, [ID1, ID2 | Rest]) ->
    initEdges(adtgraph:setAtE(Graph, ID1, ID2, flow, 0), Rest).

initVertex(Graph, []) -> Graph;

initVertex(Graph, [ID | Rest]) ->
    initVertex(adtgraph:setAtV(Graph, ID, weight, {nil, inf, false}), Rest).


initGraph(Graph) ->
%%Vorgänger und delta in allen Knoten speichern
%% Alle Kanten des Graphens Fluss zuweisen
%%
    initVertex(initEdges(Graph, adtgraph:getEdges(Graph)), adtgraph:getVertices(Graph)).


addListToStack([], Element) -> Element;

addListToStack(List, Element) -> Element++List.




forwardConditionCheck(_Graph, _ID1, [], IDList) ->
    IDList;


forwardConditionCheck(Graph, ID1, [ID2 | Rest], IDList) ->
%%(1) Für vom zu inspizierenden Knoten ausgehenden Vorwärtskanten    „flow“ < „weight“
    case adtgraph:getValV(Graph, ID2, weight) of
        {_, _, false} ->
            case adtgraph:getValE(Graph, ID1, ID2, flow) < adtgraph:getValE(Graph, ID1, ID2, weight) of
                true -> forwardConditionCheck(Graph, ID1, Rest, [ID2 | IDList]);
                _Else -> forwardConditionCheck(Graph, ID1, Rest, IDList)
            end;
        {_, _, true} -> forwardConditionCheck(Graph, ID1, Rest, IDList)
    end.



backwardConditionCheck(_Graph, _ID1, [], IDList) -> IDList;

backwardConditionCheck(Graph, ID1, [ID2 | Rest], IDList) ->
    %%(2) Für zum Knoten hinführende Rückwärtskanten „flow“ > 0
    case adtgraph:getValV(Graph, ID2, weight) of
        {_, _, false} ->
            case adtgraph:getValE(Graph, ID2, ID1, flow) > 0 of
                true -> backwardConditionCheck(Graph, ID1, Rest, [ID2 | IDList]);
                _Else -> backwardConditionCheck(Graph, ID1, Rest, IDList)
            end;
        {_, _, true} -> backwardConditionCheck(Graph, ID1, Rest, IDList)
    end.


%%% @doc returns a new Graph where the Edges

%%Markiere die gefunden Knoten wie folgt:

%% @doc (1) Für Vorwärtskanten:
%%Lege den
%%speichere die ID aktuellen Knoten als neuen „vorg“.
%%Speichere die kleinere der beiden Zahlen „weight“ – „flow“ und „delta“ des aktuellen Knotens als neues „delta“.

markVertexForward(Graph, _ID1, []) -> Graph;

markVertexForward(Graph, ID1, [ID2 | List]) ->
    Flow = adtgraph:getValE(Graph, ID1, ID2, flow),
    Weight = adtgraph:getValE(Graph, ID1, ID2, weight),
    {_Vorg, Delta, _} = adtgraph:getValV(Graph, ID1, weight),
    Buff = (Weight - Flow),
    case Buff < Delta of
        true -> markVertexForward(adtgraph:setAtV(Graph, ID2, weight, {ID1, Buff, false}), ID1, List);
        _Else -> markVertexForward(adtgraph:setAtV(Graph, ID2, weight, {ID1, Delta, false}), ID1, List)
    end.


%% @doc (2) Für Rückwärtskanten:
%%speichere die ID aktuellen Knoten *-1 als neuen „vorg“.
%%speichere die kleinere der beiden Zahlen „flow“ und “delta“ des aktuellen Knotens als neues „delta“.

markVertexBackward(Graph, _ID1, []) -> Graph;

markVertexBackward(Graph, ID1, [ID2 | List]) ->
    {_Vorg, Delta, _} = adtgraph:getValV(Graph, ID1, weight),
    Flow = adtgraph:getValE(Graph, ID2, ID1, flow),
    case Flow < Delta of
        true -> markVertexBackward(adtgraph:setAtV(Graph, ID2, weight, {ID1 * -1, Flow, false}), ID1, List);
        _Else -> markVertexBackward(adtgraph:setAtV(Graph, ID2, weight, {ID1 * -1, Delta, false}), ID1, List)
    end.


%% @doc Finde die Nachbarn des Knotens (getTarget, getSource)

getNeightbours(Graph, ID) ->
    %% @doc  Finde die Nachbarn des Knotens (getTarget, getSource)
    ForwardEdges = forwardConditionCheck(Graph, ID, adtgraph:getTarget(Graph, ID), []),
    BackwardEdges = backwardConditionCheck(Graph, ID, adtgraph:getSource(Graph, ID), []),
    {ForwardEdges, BackwardEdges}.


targetIsPart(_Target, []) -> false;

targetIsPart(Target, [ID | _Rest]) when Target == ID -> true;

targetIsPart(Target, [_ID | Rest]) -> targetIsPart(Target, Rest).


%% @doc End Pathfinding and return Status false
fordfulkerson_rec2(Graph, _Target, []) -> {Graph, false};

%% @doc find path until target is found and set delta everytime, if target is found->backrecursiv setting flow and return Status true
fordfulkerson_rec2(PGraph, Target, [InspectedVertex | Rest]) ->
    {Vorg, Delta, _} = adtgraph:getValV(PGraph, InspectedVertex, weight),
    Graph = adtgraph:setAtV(PGraph, InspectedVertex, weight, {Vorg, Delta, true}),
%%% @doc Nimm einen Knoten vom Stapel und inspiziere ihn wie folgt:
    {ForwardEdges, BackwardEdges} = getNeightbours(Graph, InspectedVertex),

    NewGraph = markVertexForward(markVertexBackward(Graph, InspectedVertex, BackwardEdges), InspectedVertex, ForwardEdges),

    case targetIsPart(Target, ForwardEdges) or targetIsPart(Target, BackwardEdges) of
        true -> {NewGraph, true};                  %%% @doc Path found
        _Else ->%%% @doc Path not found, addListToStack and choose rec of fordfulkerson
            NewStack = addListToStack(addListToStack(Rest, ForwardEdges) ,BackwardEdges),
            fordfulkerson_rec2(NewGraph, Target, NewStack)
    end.



%% @doc
%%4) Vergrößerung der Flussstärke
%%  a) Von s ausgehend, bis q erreicht wurde:
%%    i) Betrachte den „vorg“ des Knotens
%%      (1) Speichere den „vorg“ in einer Log Datei „VerGrWege.log“,
%%      (2) Für jede Vorwärtskante im Weg:
%%          (a) Addiere das „delta“ von s auf den „flow“ der Kante zum „vorg“
%%      (3) Für jede Rückwärtskante im Weg:
%%          (a) Subtrahiere das „delta“ von s vom „flow“ der Kante zum „vorg“
%%  b) Speichere das „delta“ von s in der Log Datei „VerGrWege.log“
%%  c) Gehe mit dem so modifizierten Graphen zu 3.

getPathAndSetFlow(Graph, Source, Vertex, _DeltaOfTarget, Path) when Source == Vertex ->
    NewPath = [Vertex | Path],
    {Graph, NewPath};

getPathAndSetFlow(Graph, Source, Vertex, DeltaOfTarget, Path) ->
    {Vorg, _Delta, _} = adtgraph:getValV(Graph, Vertex, weight),
    case Vorg >= 0 of
        true ->  %%Vorwärtskante
            NewPath = [Vertex | Path],
            OldFlow = adtgraph:getValE(Graph, Vorg, Vertex, flow),
            NewGraph = adtgraph:setAtE(Graph, Vorg, Vertex, flow, OldFlow + DeltaOfTarget),
            getPathAndSetFlow(NewGraph, Source, Vorg, DeltaOfTarget, NewPath);
        _Else ->   %%Rückwärtskante
            NewPath = [Vertex | Path],
            OldFlow = adtgraph:getValE(Graph, Vertex, Vorg * -1, flow),
            NewGraph = adtgraph:setAtE(Graph, Vertex, Vorg * -1, flow, OldFlow - DeltaOfTarget),
            getPathAndSetFlow(NewGraph, Source, Vorg * -1, DeltaOfTarget, NewPath)
    end.


writePathToLog(File, [], DeltaOfTarget) -> io:fwrite(File, "With Delta of: ~p ~n", [DeltaOfTarget]);

writePathToLog(File, [ID | Rest], DeltaOfTarget) ->
    io:fwrite(File, " ~p ,", [ID]),
    writePathToLog(File, Rest, DeltaOfTarget).



resetVisited(Graph, []) -> Graph;

resetVisited(Graph, [ID | Rest]) ->
    {Vorg, Delta, _} = adtgraph:getValV(Graph, ID, weight),
    resetVisited(adtgraph:setAtV(Graph, ID, weight, {Vorg, Delta, false}), Rest).



fordfulkerson_rec1(Graph, Source, Target, File, LastPath,NameAndTimeStamp) ->
    {NGraph, Status} = fordfulkerson_rec2(Graph, Target, [Source]),
    NewGraph = resetVisited(NGraph, adtgraph:getVertices(NGraph)),

    %%Status is a boolean true->Pathfound and modificated, false->Path not found->End of Algorithm
    case Status of
        true ->

            Return = adtgraph:getValV(NewGraph, Target, weight),

            {_Vorg, DeltaOfTarget, _} = Return,



            {NewNewGraph, Path} = getPathAndSetFlow(NewGraph, Source, Target, DeltaOfTarget, []),

            writePathToLog(File, Path, DeltaOfTarget),


            fordfulkerson_rec1(NewNewGraph, Source, Target, File, Path,NameAndTimeStamp);%%doNotEndAlgorithm, build Path from Target, Print PathToLog And fordfulkerson_rec1
        false ->


            adtgraph:printGFF(Graph, list_to_atom(NameAndTimeStamp)),


            LastPath
    end.


fordfulkerson_rec1(Graph, Source, Target, LastPath) ->
    {NGraph, Status} = fordfulkerson_rec2(Graph, Target, [Source]),
    NewGraph = resetVisited(NGraph, adtgraph:getVertices(NGraph)),

    %%Status is a boolean true->Pathfound and modificated, false->Path not found->End of Algorithm
    case Status of
        true ->
            {_Vorg, DeltaOfTarget, _} = adtgraph:getValV(NewGraph, Target, weight),
            {NewNewGraph, Path} = getPathAndSetFlow(NewGraph, Source, Target, DeltaOfTarget, []),
            fordfulkerson_rec1(NewNewGraph, Source, Target, Path);%%doNotEndAlgorithm, build Path from Target, Print PathToLog And fordfulkerson_rec1
        false ->
            LastPath

    end.


fordfulkerson(PreGraph, Source, Target) ->
    case util:type_is(PreGraph) of
        atom -> Graph = initGraph(adtgraph:importG(PreGraph, d));
        tuple -> Graph = initGraph(PreGraph)
    end,
    {_,_,TimeStamp}=erlang:timestamp(),
    NameAndTimeStamp="VerGrWege"++util:to_String(TimeStamp),

    {ok, File} = file:open(io_lib:format("~s",[NameAndTimeStamp++".log"]), [write]),
    %%Open Log Data and give it as argument to func fordfulkerson_rec1
    LastPath = fordfulkerson_rec1(Graph, Source, Target, File, [],NameAndTimeStamp),
    file:close(File),
    LastPath.




fordfulkersonT(PreGraph, Source, Target) ->
    case util:type_is(PreGraph) of
        atom -> Graph = initGraph(adtgraph:importG(PreGraph, d));
        tuple -> Graph = initGraph(PreGraph)
    end,
    fordfulkerson_rec1(Graph, Source, Target, []).
