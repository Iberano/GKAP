%%%-------------------------------------------------------------------
%%% @author Andre Soblechero, Eike Meyer
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. Apr 2018 12:26
%%%-------------------------------------------------------------------
-module(bellmannford).
-author("Andre Soblechero").
-author("Eike Meyer").

%% API
-export([bellmannford/3, benchmark/5, benchmark/4]).


buildClosedGraph(Graph, []) -> Graph;

buildClosedGraph(Graph, [A | Rest]) ->
  NewGraph = adtgraph:setAtV(Graph, A, weight, {nil, inf}),  %%{nil, inf}-> {Vorgänger und Entfernung vom Startpunkt}
  buildClosedGraph(NewGraph, Rest).


% [Vertex,Entfernung, Vorgänger]

buildOutput(_Graph, []) -> [];

buildOutput(Graph, [A | Rest]) ->
  {Source, Entf} = adtgraph:getValV(Graph, A, weight),
  [{A, Entf, Source} | buildOutput(Graph, Rest)].



iteration(Graph, []) -> Graph;


iteration(Graph, [ID1, ID2 | Rest]) ->

  {Source, Weight} = adtgraph:getValV(Graph, ID1, weight), %Bei ID1 nachschauen ob er eine Source hat,gege. setzen ansonsten goTO next
  {Source2, Weight2} = adtgraph:getValV(Graph, ID2, weight),
  case util:getglobalvar(yndirected) of   %Unterscheidung ob Graph directed oder undirected
    d ->
      case Source == nil of                    %Gucke ob ID1 bereits einen Vorgänger hat

        true -> iteration(Graph, Rest);  %Es gibt keinen Vorgänger eingetragen->Gehe zum nächsten Edge
        _Else ->                        %Es gibt einen Vorgänger eingetrage->
          case Source2 == nil of
            true ->
              WeightOfEdge = adtgraph:getValE(Graph, ID1, ID2, weight),
              NewWeight = Weight + WeightOfEdge,   %So berechne das gewicht zum Knoten ID2 und gebe dort vorgänger an
              NewGraph = adtgraph:setAtV(Graph, ID2, weight, {ID1, NewWeight}),
                      %und setzte die ChangeFlag auf true
              iteration(NewGraph, Rest);
            _Else ->
              WeightOfEdge = adtgraph:getValE(Graph, ID1, ID2, weight),
              NewWeight = Weight + WeightOfEdge,

              case NewWeight >= Weight2 of       %sofern ID2 nicht einen kleineren weight bereits besitzt.
                true -> iteration(Graph, Rest);
                _Else ->
                  NewGraph = adtgraph:setAtV(Graph, ID2, weight, {ID1, NewWeight}),

                  iteration(NewGraph, Rest)
              end
          end
      end;
    ud ->  %Undirected
      %io:fwrite("~nUntersuche folgende IDs: ~p   ~p~n~n~n~p~n~n", [ID1, ID2, Graph]),


      case Source == nil of    %%TODO ENDLOSSCHLEIFE
        true -> case Source2 == nil of   %(Source1=nil und Source2 = nil)
                  true -> iteration(Graph, Rest); %Goto next
                  _Else ->
                    WeightOfEdge1 = adtgraph:getValE(Graph, ID1, ID2, weight),
                    NewWeightPlusEdge2 = Weight2 + WeightOfEdge1,
                    NewGraph = adtgraph:setAtV(Graph, ID1, weight, {ID2, NewWeightPlusEdge2}),

                    iteration(NewGraph, Rest)
                end;
        _Else ->
          case Source2 == nil of
            true ->
              WeightOfEdge1 = adtgraph:getValE(Graph, ID1, ID2, weight),
              NewWeightPlusEdge1 = Weight + WeightOfEdge1,
              NewGraph = adtgraph:setAtV(Graph, ID2, weight, {ID1, NewWeightPlusEdge1}),

              iteration(NewGraph, Rest);
            _Else -> %Source1 !=nil Source2=nil
              WeightOfEdge1 = adtgraph:getValE(Graph, ID1, ID2, weight),
              NewWeightPlusEdge1 = Weight + WeightOfEdge1,  %25+1
              NewWeightPlusEdge2 = Weight2 + WeightOfEdge1, %1+1
              %io:fwrite("~nWeight ID1: ~p  ID2: ~p~n", [NewWeightPlusEdge1, NewWeightPlusEdge2]),
              case Weight == Weight2 of
                true -> iteration(Graph, Rest); %Goto next;  %nextIter
                _Else ->      %setRight%%TODO baue case drüber, damit noch weight mit weightshit usw blabla
                  case NewWeightPlusEdge1 < Weight2 of
                    true ->
                      NewGraph = adtgraph:setAtV(Graph, ID2, weight, {ID1, NewWeightPlusEdge1}),

                      iteration(NewGraph, Rest);
                    _Else ->
                      case NewWeightPlusEdge2 < Weight of
                        true ->
                          NewGraph = adtgraph:setAtV(Graph, ID1, weight, {ID2, NewWeightPlusEdge2}),

                          iteration(NewGraph, Rest);
                        _Else -> iteration(Graph, Rest)
                      end
                  end
              end
          end
      end
  end.


checkForNegativeCircles(_, []) -> false;

checkForNegativeCircles(Graph, [ID1, ID2 | Rest]) ->
  {_Source, Weight} = adtgraph:getValV(Graph, ID1, weight), %Bei ID1 nachschauen ob er eine Source hat,gege. setzen ansonsten goTO next
  {_Source2, Weight2} = adtgraph:getValV(Graph, ID2, weight),
  WeightEdge = adtgraph:getValE(Graph, ID1, ID2, weight),
  %io:fwrite("~p   ~p",[WeightEdge,Weight ]),
  case Weight ==inf of
    true->checkForNegativeCircles(Graph, Rest);
    _Else-> WeightPlusEdge = Weight + WeightEdge,
             case WeightPlusEdge < Weight2 of
               true -> true;
               _Else -> checkForNegativeCircles(Graph, Rest)
             end
  end

.

%dfkjhdsjjj


bellmannford(Graph, EdgeList, _DUD, VertexList, Index) when Index == 0 ->
  Bool = checkForNegativeCircles(Graph, EdgeList),

  case Bool of
    true -> "Ziklus negativer Länge gefunden";
    _Else -> buildOutput(Graph, VertexList)
  end;

bellmannford(Graph, EdgeList, DUD, VertexList, Index) ->
  util:setglobalvar(flag, false),

  NewGraph = iteration(Graph, EdgeList),
  %io:fwrite("Test"),
%  case util:getglobalvar(flag) of
%    true ->
  bellmannford(NewGraph, EdgeList, DUD, VertexList, Index - 1)                %Es wurde was geändert, so iteriere nochmal
%    _Else ->
%      checkForNegativeCircles(NewGraph,EdgeList),
%      buildOutput(NewGraph, VertexList)                    %Es wurde nichts geändert wo ist der Algo zuende und baue nun anhand des Baumes die Rückgabe
%  end
.

%asdesfddasfköä

bellmannford(PreGraph, StartVertex, DUD) ->

  case util:type_is(PreGraph) of
    atom -> Graph = adtgraph:importG(PreGraph, DUD);
    _Else -> Graph = PreGraph
  end,

  EdgesList = adtgraph:getEdges(Graph),


  VertexList = adtgraph:getVertices(Graph),
  Index = length(VertexList) - 2,
  NewGraph = buildClosedGraph(Graph, VertexList), %%Sets all edges attributes and names to <nil>+
  ClosedGraph = adtgraph:setAtV(NewGraph, StartVertex, weight, {StartVertex, 0}),
  util:setglobalvar(flag, true),

  %io:fwrite("Vertexlist: ~p~n", [VertexList]),
 % io:fwrite("Vertexlist: ~p~n", [EdgesList]),

  NewNewGraph = iteration(ClosedGraph, EdgesList),
 % io:fwrite("~p", [NewNewGraph]),
  bellmannford(NewNewGraph, EdgesList, DUD, VertexList, Index).




getMeasuredMeanValue(_NewTree, _RandList, _DUD, Index,Last) when Index == 3 -> Last;

getMeasuredMeanValue(NewTree, RandList, DUD, Index,Last) ->
  RandElemGT = lists:nth(rand:uniform(length(RandList)), RandList),
  {GetTargetTime, _} = timer:tc(bellmannford, bellmannford, [NewTree, RandElemGT, DUD]),
  getMeasuredMeanValue(NewTree, RandList, DUD, Index + 1,Last+GetTargetTime).


benchmark(_X, _Y, 0, _DUD, _ResultsFile) -> ok;

benchmark(X, Y, INDEX, DUD, ResultsFile) ->
%generate Graph
  XX=round(X/10),
  try gengraph:gengraph(X, 1, XX, 1, XX, "isse.graph")
  catch
    ok->io:fwrite("Gengraph")

  end,
  io:fwrite("~p~n",[DUD]),
  NewTree = adtgraph:importG(isse, DUD),
  Edges = adtgraph:getEdges(NewTree),
  %measure runtime
  EdgesSum = length(Edges) / 2,
  VertexSum = length(adtgraph:getVertices(NewTree)),

  %io:fwrite("~nVetexSum: ~p EdgesSum: ~p~n",[VertexSum,EdgesSum]),
  io:fwrite("Start measuring...~n"),
  MeanValue = getMeasuredMeanValue(NewTree, Edges, DUD, 0,0) / 3,
%get |Edges|,|Vertex|

  %Calculate predicted runtime
  PredictedTime = EdgesSum * VertexSum * VertexSum ,
  io:fwrite(ResultsFile, "~p,~p,~p,~p~n", [ VertexSum,EdgesSum, PredictedTime, MeanValue]),
  benchmark(X + Y, Y, INDEX - 1,  DUD, ResultsFile).



%gh


benchmark(X, Y, INDEX,  ud) ->
  DUD = ud,
  {ok, ResultsFile} = file:open("Bellman_undirected_results.csv", [write]),
  io:fwrite(ResultsFile, "|Vertecies|,|Edges|,erwartete Laufzeit,durchschnittliche Laufzeit~n", []),
  benchmark(X, Y, INDEX,  DUD, ResultsFile),
  file:close(ResultsFile);


benchmark(X, Y, INDEX,  d) ->
  DUD = d,
  {ok, ResultsFile} = file:open("Bellman_directed_results.csv", [write]),
  io:fwrite(ResultsFile, "|Vertecies|,|Edges|,erwartete Laufzeit,durchschnittliche Laufzeit~n", []),
  benchmark(X, Y, INDEX,DUD, ResultsFile),
  file:close(ResultsFile).