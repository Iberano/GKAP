@Author Eike Meyer
@Author André Soblechero

folgende Module werden benötigt und müssen kompiliert werden:
util.erl
fordfulkerson.erl
edmondskarp.erl
adtgraph.erl

Folgende Schnitstellen wurden zu verfügung gestellt:
fordfulkerson:fordfulkerson(<X>,<Quelle>,<Senke>)
fordfulkerson:fordfulkersonT(<X>,<Quelle>,<Senke>)

edmondskarp:edmondskarp(<X>,<Quelle>,<Senke>)
edmondskarp:edmondskarpT(<X>,<Quelle>,<Senke>)

Das <X> ist als ein zuvor gebauter Graph (adtgraph, nur directed) oder ein Atom,
welches der Name einer im gleichen Verzeichnis liegenden Datei sein muss aus welcher der Graph eingelesen werden soll, zu verstehen.


Das Programm muss von einer Commandline oder Erlangshell aus gestartet werden, welche sich im gleichen Verzeichnis befindet. 